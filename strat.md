# Secret room strat (09/04/2023 v1.0)

**Il n'y a pas besoin de 10 millions à l'entrée même si c'est marqué :kek:**

2 phases : 
 - Les couloirs
 - Le boss

## Map

![secret room map](map_strat.png)

## Les couloirs
Après 1 min, les monstres vont commencer à spawn dans toutes les zones, cependant ils ne spawnent pas tous instantanément, il faudra collecter pendant environ 40 secondes le temps qu'ils soient tous apparus. Pratiquement tous sont collectables par n'importe qui, dans les premières zones donc n'hésitez pas à aggro ceux qui spawnent à côté de vous.
>Attention à ne pas en oublier en chemin ! Si cela se produit il faudra revenir en arrière. Laissez 1 ou 2 dps trainer à l'autre bout du couloir pour être sûr de tous les attraper.

### Stratégies
3 parties pour les 24 joueurs, référencées party A, party B, party C.

Chaque membre se verra assigner une party, selon le nombre de parties la stratégie peut varier.

L'idée est de regrouper les mobs de la zone X1 avec ceux de la zone X2 à l'intersection des 2 zones.

Ainsi si un groupe se voit assigner la zone A, il devra se répartir pour collecter les zones A1 et A2.
>Si un groupe se voit assigner la zone A1 précisément, il devra collecter les monstres de la zone A1 mais quand même se grouper sur l'intersection des 2 zones A1 et A2 pour tuer

---
### Stratégie 1 (si peu de joueurs ou des parties pas équilibrés)
*dans ce scénario, la party B ou C peuvent ne pas exister*

    Les 3 parties forment une seule partie, les zones sont clear successivement (A->B->C->D). Le groupe se sépare afin de collecter les monstres des 2 sous-zones 1 et 2 à chaque fois.

### Stratégie 2 (24 joueurs avec des parties équilibrées)
    La party A clear la zone A, la party B la B et la C la ... C.

    Les 3 parties se rejoignent au fur et à mesure (A finit et vient aider B, puis les 2 rejoignent C et tout le monde finit D ensemble) -> si une party finit avant elle avance.

    Les joueurs des zones C et B doivent s'organiser pour être les plus rapidement dans leurs zones (c'est également valable pour les joueurs de la zone A2) : ne pas hésiter à utiliser un scroll of sprint.

    La party A devrait être composée des joueurs de plus bas niveau car les monstres de la zone A sont les plus faibles. Il faut construire les parties en prenant compte de l'augmentation des niveaux des zones : D > C > B > A

    Les RMs pourront buff pendant la collecte des mobs (prioriser "pas léger(quick step)" sur tout le monde afin d'atteindre sa zone rapidement). On a 1 min pour rejoindre la zone et commencer la collecte et à buff.


**Communiquez efficacement avant d'entrer dans l'instance et repérez bien votre zone et votre groupe**

## Le Boss (Asmodan)

Spawn des adds "Captain Nyx" qui spawnent à leurs tours des "Nyx" et eux-même des "Small Nyx". Il est donc extrêmement important de tuer les Captains au spawn avant d'avoir plus d'adds. **Si vous êtes < 138** NE TOUCHEZ PAS LES ADDS ils vont vous OS.
>Target priority : `Captain Nyx > Nyx > Small Nyx > Boss`

Outre cette "mécanique", le boss possède également un set de quelques skills qu'il caste dans un ordre aléatoire et sans prévenir. Les mécaniques sont les suivantes :

![asmodan skills](boss_skills.png)

### Rôles
**Tanks**

Le plus tanky possible, vraiment. Le boss fait très mal et en rage vraiment trop.
 * **Main tank (MT)** :  Spam Guard **on cooldown**. Ne pas bouger et spam le taunt dès que le boss se retourne.
 * **Offtank(s) (OT)** : cast Heart of Sacrifice sur le MT on cooldown (se synchroniser si >1 OT). Rester en range des heals afin d'encaisser les dégâts reçus de heart of sacrifice et être prêt à tout instant à reprendre la place du MT en cas de mort de ce dernier.

**Heal**
 * **Tank team** : Au moins 2 ou 3 healers dédiés au soutien des tanks. Se synchroniser pour spam Holyguard et Barrier of life sur le MT. Res et rebuff uniquement les tanks et spam heal rain.
 * **Melee team** : le boss applique un debuff sur les melees qui leur fait subir de lourds dégâts (avec Poison Cloud). Clarté ce debuff dès que possible. Pas vraiment de RM assigné à cette tâche mais surveillez tous les melees de votre party.
 * **Res / rebuff** : un RM pour chaque 2 melee par party environ, dédié à res et rebuff les ~~melees~~ morts. Généralement ceux avec les meilleurs buffs / cast speed. En cas d'hécatombe, demander explicitement de l'aide.
 * **Holycross** : si vous n'avez rien d'autre décrit au-dessus à faire, go croix le boss à fond.

**DPS**
 * **Melees** : Ouch. Le boss va vous oneshot, désolé mais c'est comme ça. Attendez gentiment que le RM vous res. Pour le reste vous êtes toujours un atout au dps donc ne négligez pas votre rôle. Outre les oneshots, vous allez aussi manger un debuff de temps en temps, il tape à environ 15k par hit. Essayez d'utiliser un antidote faites moi un retour je suis curieux. Sinon demander clarté à votre RM (cf au-dessus Heal#melee team) et priez.
 * **Ranges** : easy fight attention quand même en cas de récupération d'aggro à ne pas s'écarter trop pour éviter que le boss reset. Les YJ attention aussi à ne pas vous retrouver en melee range.
 * **Mages** : Lors de l'apparition d'adds, spammez vos aoes sur le boss pour les tuer facilement.